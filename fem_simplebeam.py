import os

import feminout.importVTKResults
from femtools import ccxtools
import FreeCAD as App
import ObjectsFem


print('start')

name = 'fem_simplebeam'
doc = App.newDocument(name)

box = doc.addObject('Part::Box', 'Box')
box.Length = 100
box.Width = 20
box.Height = 10
doc.recompute()

analysis = ObjectsFem.makeAnalysis(doc, 'Analysis')

solverCcxTools = ObjectsFem.makeSolverCalculixCcxTools(doc)
analysis.addObject(solverCcxTools)

material = ObjectsFem.makeMaterialSolid(doc)
material_parameters = {
    'AuthorAndLicense': '(c) 2013 Juergen Riegel (CC-BY 3.0)',
    'CardName': 'Calculix-Steel',
    'Density': '7900 kg/m^3',
    'Description': 'Standard steel material for Calculix sample calculations',
    'Father': 'Metal',
    'Name': 'Calculix-Steel',
    'PoissonRatio': '0.3',
    'SpecificHeat': '590 J/kg/K',
    'ThermalConductivity': '43 W/m/K',
    'ThermalExpansionCoefficient': '0.000012 m/m/K',
    'YoungsModulus': '210000 MPa'
}
material.Material = material_parameters
analysis.addObject(material)

constraintFixed = doc.addObject('Fem::ConstraintFixed', 'ConstraintFixed')
constraintFixed.Scale = 1
constraintFixed.References = [box, 'Face1']
analysis.addObject(constraintFixed)
doc.recompute()

constraintForce = doc.addObject('Fem::ConstraintForce','ConstraintForce')
constraintForce.Force = 500
constraintForce.Direction = (App.ActiveDocument.Box,['Edge5'])
constraintForce.Reversed = True
constraintForce.Scale = 1
constraintForce.References = [(App.ActiveDocument.Box,'Face2')]
analysis.addObject(constraintForce)
doc.recompute()

mesh = ObjectsFem.makeMeshNetgen(doc, 'FEMMeshNetgen')
mesh.Shape = box
mesh.MaxSize = 5.0
analysis.addObject(mesh)
doc.recompute()

fea = ccxtools.FemToolsCcx()
fea.purge_results()
fea.run()

ccxResults = doc.CCX_Results
stress_max = max(ccxResults.vonMises)
stress_min = min(ccxResults.vonMises)

print(stress_max)
print(stress_min)

dir = os.path.dirname(__file__)
feminout.importVTKResults.export([ccxResults], f'{dir}/{name}-CCX_Results_Mesh.vtk')

doc.saveAs(f'{dir}/{name}.FCStd')

print('end')
