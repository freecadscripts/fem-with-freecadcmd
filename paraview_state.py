# state file generated using paraview version 5.10.0-RC1

# *** NOTE: THIS FILE IS MODIFIED BY M.TANAKA *** 
import os

# uncomment the following three lines to ensure this script works in future versions
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 10

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1133, 728]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [50.0, 10.0, 5.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [170.30557113071865, -98.0948672956271, 36.191657160315934]
renderView1.CameraFocalPoint = [62.09389783818497, 16.18891377630145, -8.46683344032345]
renderView1.CameraViewUp = [-0.24873698215207113, 0.13904547414132584, 0.9585386115492137]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 51.234753829797995

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1133, 728)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Legacy VTK Reader'
dir = os.path.dirname(__file__)
fem_simplebeamCCX_Results_Meshvtk = LegacyVTKReader(registrationName='fem_simplebeam-CCX_Results_Mesh.vtk', FileNames=[f'{dir}/fem_simplebeam-CCX_Results_Mesh.vtk'])

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(registrationName='WarpByVector1', Input=fem_simplebeamCCX_Results_Meshvtk)
warpByVector1.Vectors = ['POINTS', 'Displacement']
warpByVector1.ScaleFactor = 50000.0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from fem_simplebeamCCX_Results_Meshvtk
fem_simplebeamCCX_Results_MeshvtkDisplay = Show(fem_simplebeamCCX_Results_Meshvtk, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'vonMisesStress'
vonMisesStressLUT = GetColorTransferFunction('vonMisesStress')
vonMisesStressLUT.RGBPoints = [1485646.1287, 0.231373, 0.298039, 0.752941, 74967366.02435, 0.865003, 0.865003, 0.865003, 148449085.92, 0.705882, 0.0156863, 0.14902]
vonMisesStressLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'vonMisesStress'
vonMisesStressPWF = GetOpacityTransferFunction('vonMisesStress')
vonMisesStressPWF.Points = [1485646.1287, 0.0, 0.5, 0.0, 148449085.92, 1.0, 0.5, 0.0]
vonMisesStressPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
fem_simplebeamCCX_Results_MeshvtkDisplay.Representation = 'Surface With Edges'
fem_simplebeamCCX_Results_MeshvtkDisplay.ColorArrayName = ['POINTS', 'von Mises Stress']
fem_simplebeamCCX_Results_MeshvtkDisplay.LookupTable = vonMisesStressLUT
fem_simplebeamCCX_Results_MeshvtkDisplay.Opacity = 0.5
fem_simplebeamCCX_Results_MeshvtkDisplay.SelectTCoordArray = 'None'
fem_simplebeamCCX_Results_MeshvtkDisplay.SelectNormalArray = 'None'
fem_simplebeamCCX_Results_MeshvtkDisplay.SelectTangentArray = 'None'
fem_simplebeamCCX_Results_MeshvtkDisplay.OSPRayScaleArray = 'Displacement'
fem_simplebeamCCX_Results_MeshvtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
fem_simplebeamCCX_Results_MeshvtkDisplay.SelectOrientationVectors = 'Displacement'
fem_simplebeamCCX_Results_MeshvtkDisplay.ScaleFactor = 10.0
fem_simplebeamCCX_Results_MeshvtkDisplay.SelectScaleArray = 'Displacement'
fem_simplebeamCCX_Results_MeshvtkDisplay.GlyphType = 'Arrow'
fem_simplebeamCCX_Results_MeshvtkDisplay.GlyphTableIndexArray = 'Displacement'
fem_simplebeamCCX_Results_MeshvtkDisplay.GaussianRadius = 0.5
fem_simplebeamCCX_Results_MeshvtkDisplay.SetScaleArray = ['POINTS', 'Displacement']
fem_simplebeamCCX_Results_MeshvtkDisplay.ScaleTransferFunction = 'PiecewiseFunction'
fem_simplebeamCCX_Results_MeshvtkDisplay.OpacityArray = ['POINTS', 'Displacement']
fem_simplebeamCCX_Results_MeshvtkDisplay.OpacityTransferFunction = 'PiecewiseFunction'
fem_simplebeamCCX_Results_MeshvtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
fem_simplebeamCCX_Results_MeshvtkDisplay.PolarAxes = 'PolarAxesRepresentation'
fem_simplebeamCCX_Results_MeshvtkDisplay.ScalarOpacityFunction = vonMisesStressPWF
fem_simplebeamCCX_Results_MeshvtkDisplay.ScalarOpacityUnitDistance = 11.401161807328219
fem_simplebeamCCX_Results_MeshvtkDisplay.OpacityArrayName = ['POINTS', 'Displacement']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
fem_simplebeamCCX_Results_MeshvtkDisplay.ScaleTransferFunction.Points = [-3.53595e-05, 0.0, 0.5, 0.0, 3.535919999999999e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
fem_simplebeamCCX_Results_MeshvtkDisplay.OpacityTransferFunction.Points = [-3.53595e-05, 0.0, 0.5, 0.0, 3.535919999999999e-05, 1.0, 0.5, 0.0]

# show data from warpByVector1
warpByVector1Display = Show(warpByVector1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
warpByVector1Display.Representation = 'Surface With Edges'
warpByVector1Display.ColorArrayName = ['POINTS', 'von Mises Stress']
warpByVector1Display.LookupTable = vonMisesStressLUT
warpByVector1Display.SelectTCoordArray = 'None'
warpByVector1Display.SelectNormalArray = 'None'
warpByVector1Display.SelectTangentArray = 'None'
warpByVector1Display.OSPRayScaleArray = 'Displacement'
warpByVector1Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByVector1Display.SelectOrientationVectors = 'Displacement'
warpByVector1Display.ScaleFactor = 10.000003814697266
warpByVector1Display.SelectScaleArray = 'Displacement'
warpByVector1Display.GlyphType = 'Arrow'
warpByVector1Display.GlyphTableIndexArray = 'Displacement'
warpByVector1Display.GaussianRadius = 0.5000001907348632
warpByVector1Display.SetScaleArray = ['POINTS', 'Displacement']
warpByVector1Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByVector1Display.OpacityArray = ['POINTS', 'Displacement']
warpByVector1Display.OpacityTransferFunction = 'PiecewiseFunction'
warpByVector1Display.DataAxesGrid = 'GridAxesRepresentation'
warpByVector1Display.PolarAxes = 'PolarAxesRepresentation'
warpByVector1Display.ScalarOpacityFunction = vonMisesStressPWF
warpByVector1Display.ScalarOpacityUnitDistance = 11.401171150446352
warpByVector1Display.OpacityArrayName = ['POINTS', 'Displacement']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
warpByVector1Display.ScaleTransferFunction.Points = [-3.53595e-05, 0.0, 0.5, 0.0, 3.535919999999999e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
warpByVector1Display.OpacityTransferFunction.Points = [-3.53595e-05, 0.0, 0.5, 0.0, 3.535919999999999e-05, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for vonMisesStressLUT in view renderView1
vonMisesStressLUTColorBar = GetScalarBar(vonMisesStressLUT, renderView1)
vonMisesStressLUTColorBar.Title = 'von Mises Stress'
vonMisesStressLUTColorBar.ComponentTitle = ''

# set color bar visibility
vonMisesStressLUTColorBar.Visibility = 1

# show color legend
fem_simplebeamCCX_Results_MeshvtkDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
warpByVector1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(warpByVector1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')