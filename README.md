# FEM with FreeCADCmd
M. TANAKA  
created : 2023/08/24  
modified: 2023/12/25  

[TOC]

## やりたいこと

- FreeCADによるFEM．
- GUIを含まない，スクリプトのみの形式でFEM解析結果を取得するところまで行う．
    - 等価応力の最大最小を`float`変数として取得した．
    - 解析結果をVTK形式で出力した（別途ParaViewで確認できる）．
- つまり，スクリプトを一度書けば自動化できる．
- 今後，最適化などに組み込むことを想定．

## 実行環境

Miniconda内で完結している．全部conda-forgeから．設定しなくてもFreeCADが勝手にCalculixを探してくれた．

- Miniconda (conda 23.7.3)
    - freecad (0.21.0)
    - calculix (2.21)

## 実行コマンド

```bash
freecadcmd fem_simplebeam.py
```

## 作業メモ

### ベース

こちらの記事を参考に，ほぼそのままスクリプト化した．

- [コスモスデザイン / FreeCAD FEM｜構造解析と結果の分析方法を初心者向けに解説](https://cosmosdesign2021.com/fc019fem)

### スクリプト化

基本はFreeCADでGUI操作してPythonコンソールの出力をもとに清書．
Pythonコンソールに入力してオブジェクトの持つフィールド・メソッドを検索したり．
FEMの実行だけは，この手が使えなかったので以下を参考にした．

- [FreeCAD Documentation / FEM Tutorial Python](https://wiki.freecad.org/FEM_Tutorial_Python)

### 応力値の取得について

`fem_simplebeam.py`にて

```python
stress_max = max(ccxResults.vonMises) # MPa
stress_min = min(ccxResults.vonMises) # MPa
```

という風に書いているが，調べた所`ccxResults`は他にも以下の応力値リストを持っている：

| メンバ変数名    | 内容                                                           | 次元 |
|:--------------:|:---------------------------------------------------------------|:---:|
| `vonMises`     | von Mises応力                                                   | MPa |
| `NodeStrainXX` | ひずみテンソルのxx成分．同様に`XY`,`XZ`,`YY`,`YZ`,`ZZ`まで6つある． | -   |
| `NodeStressXX` | 応力テンソルのxx成分．同様に`XY`,`XZ`,`YY`,`YZ`,`ZZ`まで6つある．  | MPa |
| `PrincipalMax` | 最大主応力．同様に，`PrincipalMed`，`PrincipalMin`もある．        | MPa |

安全率の判定に色々使えそうだ．

### 予約語に注意

最初，pyファイルの名前を`femtest`としていたが，FreeCADのライブラリ名と被っているためか（理屈は知らないが），`freecadcmd femtest.py`としても何も実行されなかった．ファイル名を変更することで問題は回避できた．

### GUI操作との結果の差異

スクリプトによる結果は，GUIで操作したときと若干異なる．`CCM_Results`をダブルクリックしても解析結果が可視化されない．以下のように再計算することで一応は可視化できる．

| ![](./images/img1_just_after_freecadcmd.png) | ![](./images/img2_make_visible.png) |
|:-:|:-:|
| 読み込み直後． | 非表示解除．`Analysis`を選んで`Space`押下． | 

| ![](./images/img3_re-calculate.png) | ![](./images/img4_just_after_re-calculation.png) |
|:-:|:-:|
| 再計算．`SolverCcxTools`を選び，図のボタンを押下． | 再計算直後． | 

| ![](./images/img5_set_field_before.png) | ![](./images/img6_set_field_after.png) |
|:-:|:-:|
| 可視化．`Pipeline_CCX_Results`をダブルクリック． | モードとフィールドを変更． | 


（追記） `> freecadcmd fem_simplebeam.py`ではそうだが，`> freecad fem_simplebeam.py`とGUIアプリに同じpyファイルを読ませると正しくツリーが生成された．今回の場合あくまでコンソールで実行したいので助けにはならないが，覚えておこう．


## ParaViewで可視化画像出力

出力したvtkファイルを読み込み，`Warp By Vector`フィルターを用いることで，画像のように可視化できる．
手作業で可視化したあとに`Save State`で作成したスクリプトが`paraview_state.py`である（絶対パスなど一部修正）．これを`Load State`すれば自動で可視化できる．

![](./images/img7_visualise_with_paraview.png)

参考：
- [Xsim / CalculiX の計算結果を ParaView で可視化する](https://www.xsim.info/articles/CalculiX/Postprocessing-with-ParaView.html)

ToDo

- 可視化結果をpng出力できると，例えば最適化計算の途中経過を確認できて安心．
